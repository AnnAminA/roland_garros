$(document).ready(function () {

     // pour le formulaire :::::::::::::::::::::::::::::::::::::

  var envoi = document.getElementById('envoi');
  var erreur =  $('#erreur');
  

  envoi.addEventListener('click', function (event) {

    event.preventDefault();

    if( ($('#nom').val() !== "Nom_Organisateur") || ($('#mdp').val() !== "MotDePasse_Organisateur") ) {
      $('#nom, #mdp').css('borderColor', 'red');
      erreur.removeClass('none');
    } else {
      $('#nom').css('color', 'green'); 
      $('#mdp').css('border', 'green');
      alert('Vous êtes bien connectés ' + $('#nom').val());
      window.location.href = 'html/home.html';
    }


  })

  // ================================================

  var valeursForm = {
    nom: $('#nom').val(),
    mdp: $('#mdp').val()
  };

  /* SAVE */
  var element = document.getElementById('seSouvenir');

  if($('#seSouvenir' === true)) {
    element.addEventListener('click', function () {
      var sauvegarde = JSON.stringify(valeursForm);
      localStorage.setItem('valeursForm', sauvegarde);
      console.log('je stocke mes id dans le localStorage');
    });
  
    /* LOAD */
    var elementLoad = document.getElementById('seSouvenir');
  
    elementLoad.addEventListener('click', function () {
      var sauvegarde = localStorage.getItem('valeursForm');
      var valeursForm = JSON.parse(sauvegarde);
      //alert('Voici vos identifiants: ' + valeursForm);
      console.log(valeursForm);
    });
  }

 
});